# wsdump

wsdump is two tools used in conjuction to dump Bandai WonderSwan carts via the console itself.

The WS source code is written for MASM. You'll need [tup](http://gittup.org/tup/) to build the source, and my [trptool](http://bitbucket.org/trap15/trptool/).

The PC source code is simple C. Though it's fairly POSIX-y, the serial code is Win32 only, so you'll have to rewrite that if you want to use it on any other system. I compile it with MSYS2, so that's all I know can work.

The WonderSwan side builds as a native WonderSwan ROM, or a WonderWitch `soft` program.

The PC software outputs skeleton YAML data to be filled out and added to my work-in-progress dump database, as well as a dump file named with the guessed SKU information.

# Cool Links

* <http://daifukkat.su/> - My website
* <https://bitbucket.org/trap15/wsdump> - This repository

# Greetz

* \#raidenii - Forever impossible

# Licensing

All contents within this repository (unless otherwise specified) are licensed
under the following terms:

The MIT License (MIT)

Copyright (c) 2016 Alex 'trap15' Marshall

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.