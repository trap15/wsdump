	.186

	INCLUDE	<top.inc>

	EXTRN	_serial_enable:proc
	EXTRN	_serial_send_buf:proc
	EXTRN	_serial_send_byte:proc
	EXTRN	_serial_recv_byte:proc

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
	ASSUME	SI:PTR Work

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	PUBLIC	_main
_main::
	MOV	AX, 02000h
	MOV	DS, AX

	CALL	_serial_enable

_dumploop:
	MOV	AL, 0FFh
	OUT	0C2h, AL

	CALL	_serial_recv_byte
	CMP	AL, 055h
	JE	_sendhdr
	CMP	AL, 033h
	JNE	_dumploop

	XOR	DX, DX
	MOV	DL, DS:[0FFFAh]

	PUSH	DS
	MOV	AX, CS
	MOV	DS, AX
	MOV	BX, size_lut
	MOV	AX, DX
	XLAT
	POP	DS
	TEST	AL, AL
	JNZ	@F
	INC	AH
@@:
	MOV	CX, AX
	MOV	DX, AX
	NEG	DX
@@:
	PUSH	CX
	MOV	AL, DL
	OUT	0C2h, AL
	INC	DX

	XOR	CX, CX
	XOR	SI, SI
	CALL	_serial_send_buf
	POP	CX
	LOOP	@B

	JMP	_dumploop
_sendhdr:
	MOV	CX, 10
	MOV	SI, 0FFF6h
	CALL	_serial_send_buf
	MOV	AX, 0FFFFh
	OUT	0C0h, AX
	OUT	0C2h, AX
	IN	AL, 0C0h
	CALL	_serial_send_byte
	IN	AL, 0C1h
	CALL	_serial_send_byte
	IN	AL, 0C2h
	CALL	_serial_send_byte
	IN	AL, 0C3h
	CALL	_serial_send_byte
	JMP	_dumploop

size_lut:
	DB	2, 4, 8, 16, 32, 48, 64, 96, 128, 0

_TEXT	ENDS

	END
