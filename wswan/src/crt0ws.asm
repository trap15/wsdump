	.186

	INCLUDE	<top.inc>

	EXTRN	_main:proc

_TEXT		SEGMENT BYTE PUBLIC 'CODE'
_TEXT		ENDS
_ENDTEXT	SEGMENT BYTE PUBLIC 'CODE'
__text_end::
_ENDTEXT	ENDS

CGROUP	GROUP	_TEXT,_ENDTEXT

_TEXT	SEGMENT BYTE PUBLIC 'CODE'
_start::
	CLI
	CLD
	MOV	AX, V_WORK_SEG
	MOV	SS, AX
	MOV	SP, V_STACK_TOP

; Copy code to RAM to facilitate cart swap
	MOV	SI, 0
	MOV	AX, CS
	MOV	DS, AX
	MOV	AX, V_CODE_SEG
	MOV	ES, AX
	MOV	DI, 0
	MOV	CX, OFFSET __text_end
	INC	CX
	SHR	CX, 1
	REP MOVSW

	PUSH	V_CODE_SEG
	PUSH	_main
	RETF

_TEXT	ENDS

; set entrypoint to _start
	END	_start
