	.186

	INCLUDE	<top.inc>

REG_SER_DATA	equ	0B1h
REG_SER_STATUS	equ	0B3h

_TEXT	SEGMENT BYTE PUBLIC 'CODE'

; void serial_enable(void);
	PUBLIC	_serial_enable
_serial_enable::
	IN	AL, REG_SER_STATUS
	AND	AL, 01Fh
	OR	AL, 0E0h
	OUT	REG_SER_STATUS, AL

	RET

; void serial_disable(void);
	PUBLIC	_serial_disable
_serial_disable::
@@:
	IN	AL, REG_SER_STATUS
	TEST	AL, 4
	JZ	@B

	AND	AL, 05Fh
	OUT	REG_SER_STATUS, AL
	RET

; void serial_send_byte(BYTE v);
	PUBLIC	_serial_send_byte
_serial_send_byte::
	MOV	BL, AL
@@:
	IN	AL, REG_SER_STATUS
	TEST	AL, 4
	JZ	@B

	MOV	AL, BL
	OUT	REG_SER_DATA, AL

	RET

; void serial_send_buf(WORD[cx] len, void far*[ds:si] ptr);
	PUBLIC	_serial_send_buf
_serial_send_buf::
@@:
	IN	AL, REG_SER_STATUS
	TEST	AL, 4
	JZ	@B

	LODSB
	OUT	REG_SER_DATA, AL
	LOOP	@B

	RET

; BYTE serial_recv_byte(void);
	PUBLIC	_serial_recv_byte
_serial_recv_byte::
	JMP	__loop_recv_byte_nohlt
__recv_overrun:
	OR	AL, 020h
	OUT	REG_SER_STATUS, AL
	JMP	__loop_recv_byte_nohlt
@@:
__loop_recv_byte_nohlt:
	IN	AL, REG_SER_STATUS
	TEST	AL, 2
	JNZ	__recv_overrun
	TEST	AL, 1
	JZ	@B

	IN	AL, REG_SER_DATA
	RET

_TEXT	ENDS

	END
