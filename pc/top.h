#ifndef TOP_H_
#define TOP_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

typedef uint8_t uint8;

#define BOOL int
#define FALSE 0
#define TRUE 1

#endif
