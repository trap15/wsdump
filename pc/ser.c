#include "top.h"

#define CINTERFACE 1
#define COBJMACROS 1
#define INITGUID 1
#define CONST_VTABLE 1

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <mmsystem.h>

#define UISER_MAX_SEND_QUEUE 8

static HANDLE _uiser_hnd;
static int _uiser_baud = 0;
char *_uiser_port = NULL;

BOOL uiser_open(const char *portname, int baud)
{
  if(portname != NULL) {
    if(portname != _uiser_port) {
      if(_uiser_port != NULL) {
        free(_uiser_port);
        _uiser_port = NULL;
      }
      _uiser_port = strdup(portname);
    }
  }

  _uiser_baud = baud;
  fprintf(stderr, "Opening %s @ %d baud\n", _uiser_port, _uiser_baud);

  _uiser_hnd = CreateFile(_uiser_port, GENERIC_READ|GENERIC_WRITE, 0,
               NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL|FILE_FLAG_NO_BUFFERING, NULL);
  if(_uiser_hnd == INVALID_HANDLE_VALUE) {
    fprintf(stderr, "Cannot open serial port %s\n", _uiser_port);
    return FALSE;
  }
  // Setup serial parameters
  DCB serparam;
  serparam.DCBlength = sizeof(serparam);
  if(GetCommState(_uiser_hnd, &serparam) == 0) {
    fprintf(stderr, "Cannot get serial port parameters from %s\n", _uiser_port);
    return FALSE;
  }
  int winbaud;
  switch(baud) {
    case 110:    winbaud = CBR_110;    break;
    case 300:    winbaud = CBR_300;    break;
    case 600:    winbaud = CBR_600;    break;
    case 1200:   winbaud = CBR_1200;   break;
    case 2400:   winbaud = CBR_2400;   break;
    case 4800:   winbaud = CBR_4800;   break;
    case 9600:   winbaud = CBR_9600;   break;
    case 14400:  winbaud = CBR_14400;  break;
    case 19200:  winbaud = CBR_19200;  break;
    case 38400:  winbaud = CBR_38400;  break;
    case 57600:  winbaud = CBR_57600;  break;
    case 115200: winbaud = CBR_115200; break;
    case 128000: winbaud = CBR_128000; break;
    case 256000: winbaud = CBR_256000; break;
    default:
      fprintf(stderr, "Invalid baud rate %d\n", baud);
      return FALSE;
  }
  serparam.BaudRate = winbaud;
  serparam.ByteSize = 8;
  serparam.StopBits = ONESTOPBIT;
  serparam.Parity = NOPARITY;
  if(SetCommState(_uiser_hnd, &serparam) == 0) {
    fprintf(stderr, "Cannot set serial port parameters to %s\n", _uiser_port);
    return FALSE;
  }

  COMMTIMEOUTS timeouts;
  GetCommTimeouts(_uiser_hnd, &timeouts);
  timeouts.ReadIntervalTimeout = MAXDWORD;
  timeouts.ReadTotalTimeoutMultiplier = 0;
  timeouts.ReadTotalTimeoutConstant = 0;
  SetCommTimeouts(_uiser_hnd, &timeouts);

  fprintf(stderr, "Opened.\n");
  return TRUE;
}

BOOL uiser_close(void)
{
  CloseHandle(_uiser_hnd);
  _uiser_hnd = NULL;

  if(_uiser_port != NULL) {
    free(_uiser_port);
    _uiser_port = NULL;
  }
  return TRUE;
}

BOOL uiser_changebaud(int baud)
{
  CloseHandle(_uiser_hnd);
  _uiser_hnd = NULL;

  return uiser_open(NULL, baud);
}

ssize_t uiser_recv(void *buf, ssize_t bytes)
{
  ssize_t bytes_recv = 0;
  uint8 *cbuf = (uint8*)buf;
  DWORD recvd;
  while(bytes_recv != bytes) {
    recvd = 0;
    ReadFile(_uiser_hnd, cbuf + bytes_recv, bytes - bytes_recv, &recvd, NULL);
    bytes_recv += recvd;
  }
  return bytes_recv;
}
ssize_t uiser_send(const void *buf, ssize_t bytes)
{
  ssize_t bytes_sent = 0;
  const uint8 *cbuf = (const uint8*)buf;
  DWORD written;
  while(bytes_sent != bytes) {
    written = 0;
    WriteFile(_uiser_hnd, cbuf + bytes_sent, bytes - bytes_sent, &written, NULL);
    bytes_sent += written;
  }
  return bytes_sent;
}
