#include "top.h"
#include <string.h>
#include "sha256.h"

BOOL uiser_open(const char *portname, int baud);
BOOL uiser_close(void);
BOOL uiser_changebaud(int baud);
ssize_t uiser_recv(void *buf, ssize_t bytes);
ssize_t uiser_send(const void *buf, ssize_t bytes);

#define MFG_COUNT 0x37
typedef struct {
  char sku[4];
  char name[64];
} MfgInfo;
const MfgInfo mfg_info[MFG_COUNT] = {
  { ""   , "", },
  { "BAN", "Bandai", },
  { "TAT", "Taito", },
  { "TMY", "Tomy", },
  { "KEX", "Koei", },
  { "DTE", "Data East", },
  { "AAE", "Asmik Ace", },
  { "MDE", "Media Entertainment", },

  { "NHB", "Nichibutsu", },
  { ""   , "", },
  { "CCJ", "Coconuts Japan", },
  { "SUM", "Sammy", },
  { "SUN", "Sunsoft", },
  { "PAW", "Mebius", },
  { "BPR", "Banpresto", },
  { ""   , "", },

  { "JLC", "Jaleco", },
  { "MGA", "Imagineer", },
  { "KNM", "Konami", },
  { ""   , "", },
  { ""   , "", },
  { ""   , "", },
  { "KBS", "Kobunsha", },
  { "BTM", "Bottom Up", },

  { "KGT", "Kaga Tech", },
  { "SRV", "Sunrise", },
  { "CFT", "Cyber Front", },
  { "MGH", "Mega House", },
  { ""   , "", },
  { "BEC", "Interbec", },
  { "NAP", "Nihon Application", },
  { "BVL", "Bandai Visual", },

  { ""   , "", },
  { "KDX", "KID", },
  { "HAL", "HAL Corporation", },
  { "YKE", "Yuki Enterprise", },
  { "OMM", "Omega Micott", },
  { "LAY", "Layup", },
  { "KDK", "Kadokawa Shoten", },
  { "SHL", "Shall Luck", },

  { "SQR", "Squaresoft", },
  { ""   , "", },
  { ""   , "", },
  { "TMC", "Tom Create", },
  { ""   , "", },
  { "NMC", "Namco", },
  { "SES", "Movic", },
  { "HTR", "E3 Staff", },

  { ""   , "", },
  { "VGD", "Vanguard", },
  { "MGT", "Megatron", },
  { "WIZ", "Wiz", },
  { ""   , "", },
  { ""   , "", },
  { "CPC", "Capcom", },
};

const size_t rom_sizes[] = {
  1*128*1024,
  2*128*1024,
  4*128*1024,
  8*128*1024,
  16*128*1024,
  24*128*1024,
  32*128*1024,
  48*128*1024,
  64*128*1024,
  128*128*1024,
};

const char *sram_sizes[] = {
  "",
  "64K",
  "256K",
  "1M",
  "2M",
  "4M",
  "?", "?", "?", "?",
  "?", "?", "?", "?",
  "?", "?",
};

const char *eep_sizes[] = {
  "",
  "1K",
  "16K",
  "?", "?",
  "8K",
  "?", "?", "?", "?",
  "?", "?", "?", "?",
  "?", "?",
};

typedef struct {
  // header
  uint8_t mfg;
  uint8_t sys;
  uint8_t game;
  uint8_t rev;
  uint8_t size;
  uint8_t save;
  uint8_t flag;
  uint8_t lsi;
  uint16_t xsum;

  uint8_t lims[4];

  // dump
  MfgInfo mfgi;
  const char *mapper;
  char sha256[65];
  char syschr;
  char guess_sku[256];
  size_t bytes;
  uint8_t *buf;
} ROMInfo;

void emit_rom_meta(ROMInfo *rom) {
#define META(str...) fprintf(stderr, str);
META("- releases:\n")
META("    %s:\n", rom->guess_sku)
META("      date: @\n")
META("  sha256:      %s\n", rom->sha256)
META("  revision:    %d\n", rom->rev)
META("  name:        @\n")
META("  engname:     @\n")
META("  publisher:   %s\n", rom->mfgi.name);
META("  developer:   @\n")
META("  region:      JPN\n")
META("  system:      %s\n", rom->sys ? "WSC" : "WS")
META("  orientation: %c\n", rom->flag & 1 ? 'V' : 'H')
META("  peripheral:\n")
META("    - @\n")
META("  board:\n")
META("    type:   @\n")
META("    serial: @\n")
META("    rom:\n")
META("      - mfg:      @\n")
META("        part:     @\n")
META("        location: @\n")
META("        package:  @\n")
META("        size:     %dMbit\n", rom->bytes / (128*1024))
META("        databus:  %d\n", rom->flag & 4 ? 16 : 8)
META("        speed:    %d cycles\n", rom->flag & 2 ? 3 : 1)
META("        datecode: @\n")
META("        date:     @\n")
META("        lotcode:  @\n")
META("    mapper:\n")
META("      - mfg:      Bandai\n")
META("        part:     %s\n", rom->mapper);
META("        location: U1\n")
META("        package:  QFP-48\n")
META("        code:     @\n")
  if(rom->save & 0x0F) {
META("    sram:\n")
META("      - mfg:      @\n")
META("        part:     @\n")
META("        location: @\n")
META("        package:  @\n")
META("        size:     %sbit\n", sram_sizes[rom->save])
META("        datecode: @\n")
META("        date:     @\n")
META("        lotcode:  @\n")
  }
  if(rom->save & 0xF0) {
META("    eeprom:\n")
META("      - mfg:      @\n")
META("        part:     @\n")
META("        location: @\n")
META("        package:  @\n")
META("        size:     %sbit\n", eep_sizes[rom->save >> 4])
META("        datecode: @\n")
META("        date:     @\n")
META("        lotcode:  @\n")
  }
  if(rom->lsi & 1) {
META("    rtc:\n")
META("      - mfg:      @\n")
META("        part:     @\n")
META("        location: @\n")
META("        package:  @\n")
META("        datecode: @\n")
META("        date:     @\n")
META("        lotcode:  @\n")
  }
  if((rom->save & 0x0F) || (rom->lsi & 1)) {
META("    battery:\n")
META("      - location: BT1\n")
META("        date:     @\n")
  }
}

void emit_rom(ROMInfo *rom, const char *dumpdir) {
  char filename[256];
  snprintf(filename, sizeof(filename)-1, "%s/%s.bin", dumpdir, rom->guess_sku);
  FILE *fp = fopen(filename, "wb");
  fwrite(rom->buf, rom->bytes, 1, fp);
  fclose(fp);
  fprintf(stderr, "Wrote to %s\n", filename);

  SHA256_CTX ctx;

  // Hash one
  unsigned char hash[32];
  sha256_init(&ctx);
  sha256_update(&ctx, rom->buf, rom->bytes);
  sha256_final(&ctx, hash);
  rom->sha256[0] = '\0';
  int i;
  for(i = 0; i < 32; i++) {
    sprintf(rom->sha256+(i*2), "%02x", hash[i]);
  }

  emit_rom_meta(rom);
}

int realmain(int argc, const char *argv[]) {
  fprintf(stderr, "wsdumprecv (C)2016 Alex \"trap15\" Marshall\n");

  const char *arg_serport;
  const char *arg_dumpdir;
  if(argc < 3) {
    fprintf(stderr, "Usage:\n\t%s serport outdir\n", argv[0]);
    return 1;
  }
  arg_serport = argv[1];
  arg_dumpdir = argv[2];

  int success = 0;
  if(!uiser_open(arg_serport, 38400)) {
    return 1;
  }

  ROMInfo rom;

  uint8_t tmp[16];
  tmp[0] = 0x55;
  uiser_send(tmp, 1);
  uiser_recv(tmp, 10);

  rom.mfg = tmp[0];
  rom.sys = tmp[1];
  rom.game = tmp[2];
  rom.rev = tmp[3];
  rom.size = tmp[4];
  rom.save = tmp[5];
  rom.flag = tmp[6];
  rom.lsi = tmp[7];
  rom.xsum = tmp[8] | (tmp[9] << 8);
  uiser_recv(rom.lims, 4);

  rom.mfgi = mfg_info[0];
  if(rom.mfg < MFG_COUNT)
    rom.mfgi = mfg_info[rom.mfg];
  if(rom.mfgi.sku[0] == '\0') {
    sprintf(rom.mfgi.sku, "%02X?", rom.mfg);
    sprintf(rom.mfgi.name, "??? (%02X)", rom.mfg);
  }
  rom.syschr = rom.sys ? 'C' : '0';
  strcpy(rom.sha256, "@");

  snprintf(rom.guess_sku, sizeof(rom.guess_sku)-1, "SWJ-%s%c%02X", rom.mfgi.sku, rom.syschr, rom.game);

  fprintf(stderr, "Guessed SKU: %s\n", rom.guess_sku);
  fprintf(stderr, "Header:\n");
  fprintf(stderr, "%02X %02X %02X %02X\n", rom.mfg, rom.sys, rom.game, rom.rev);
  fprintf(stderr, "%02X %02X %02X %02X\n", rom.size, rom.save, rom.flag, rom.lsi);
  fprintf(stderr, "%04X\n", rom.xsum);
  fprintf(stderr, "lims: %02X %02X %02X %02X\n", rom.lims[0], rom.lims[1], rom.lims[2], rom.lims[3]);
  switch(rom.lims[0]) {
    case 0x0F:
      rom.mapper = "2001";
      break;
    case 0x3F:
      rom.mapper = "2003";
      break;
    default:
      rom.mapper = "???";
      break;
  }
  if(rom.lims[0] != 0x0F) {
    fprintf(stderr, "Cart is not a 2001 mapper! Can't dump!\n");
    emit_rom_meta(&rom);
    goto _done;
  }

  fprintf(stderr, "Dump? [Y/n]: ");
  for(;;) {
    int c = fgetc(stdin);
    if(toupper(c) == 'N') {
      fprintf(stderr, "\n");
      goto _done;
    }else if(toupper(c) == 'Y' || c == '\n') {
      fprintf(stderr, "\n");
      break;
    }
  }

  rom.bytes = 0x20;
  if(rom.size < sizeof(rom_sizes))
    rom.bytes = rom_sizes[rom.size];
  rom.buf = malloc(rom.bytes);

  tmp[0] = 0x33;
  uiser_send(tmp, 1);
  size_t i, l;
#define USE_XSUM 0
#define BLOCK_SIZE 0x80
  for(i = 0; i < rom.bytes;) {
    uiser_recv(rom.buf+i, BLOCK_SIZE);
#if USE_XSUM
    uiser_recv(tmp, 2);
    uint16_t xsum = tmp[0] | (tmp[1] << 8);
    uint16_t get_xsum = 0;
    for(l = 0; l < BLOCK_SIZE; l++) {
      get_xsum += rom.buf[i+l];
    }
    if(get_xsum != xsum) {
      tmp[0] = 0xAA;
      uiser_send(tmp, 1);
      fprintf(stderr, "\rRetry @ %X\n", i);
      continue;
    }else{
      tmp[0] = 0xCC;
      uiser_send(tmp, 1);
    }
#endif
    i += BLOCK_SIZE;
    int pct = i*10000/rom.bytes;
    fprintf(stderr, "\rDumped %X/%X (%d.%02d%%)", i, rom.bytes, pct / 100, pct % 100);
  }
  success = 1;
  printf("\n\n");

_done:
  uiser_close();
  if(success) {
    emit_rom(&rom, arg_dumpdir);
  }
  return 0;
}

#if WIN32
extern int __argc;
extern const char ** __argv;
int WinMain()
{
  return realmain(__argc, __argv);
}
#else
int main(int argc, const char **argv)
{
  return realmain(argc, argv);
}
#endif
